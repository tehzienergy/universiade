$(document).on('click', '.js-anchor', function (event) {
  event.preventDefault();

  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
});

$('.front__wrapper').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: false,
  adaptiveHeight: true,
  dots: true,
  arrows: false,
  autoplay: true,
  customPaging: function (slick, index) {
    var targetImage = slick.$slides.eq(index).attr('data-img');
    return '<div style="background-image: url(' + targetImage + ' ");></div>';
  },
  responsive: [
    {
      breakpoint: 567,
      settings: {
        fade: true
      }
    }
  ]
})
