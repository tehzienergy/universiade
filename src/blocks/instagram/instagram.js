$(window).on('load', function () {
  $('.instagram__content').mCustomScrollbar({
    axis: "x",
    scrollInertia: false,
    scrollInertia: 500,
  });
})


setTimeout(function () {
  
  var number = 6;
  
  if ($(window).width() < 1200) {
    number = 4;
  }
  
  if ($(window).width() < 768) {
    number = 2;
  }
  var itemWidth = $('.instagram__content').outerWidth()/number;
  $('.instagram__item').css('width', itemWidth).css('height', itemWidth);
}, 500)
