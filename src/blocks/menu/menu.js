$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('body').toggleClass('body--fixed');
  $('.menu__content').fadeToggle();
});

$(document).on('click', function (e) {
  var modal = $('.menu, .header');
  if (!modal.is(e.target) &&
    modal.has(e.target).length === 0) {
    $('.menu__btn').removeClass('menu__btn--active');
    $('body').removeClass('body--fixed');
    $('.menu__content').fadeOut();
  }
});
