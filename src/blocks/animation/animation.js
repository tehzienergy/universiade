$(window).on("load", function () {
  
  $('body').css('opacity', 1);

  window.sr = ScrollReveal();

  sr.reveal('.header', { 
    origin: 'left',
    duration: 700,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.front', { 
    origin: 'left',
    duration: 1500,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.front__item-wrapper', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    delay: 700,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.about__item', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.cards-list__header', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.card', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  

  sr.reveal('.concept__header', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.concept__section-header', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.discipline', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__additional-dscr', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__timeline-header > *', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  
  
  sr.reveal('.concept__timeline-line', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__timeline-item', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__date', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });

  sr.reveal('.news-list', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.news', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });

  sr.reveal('.timeline__header', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.timeline__subheader', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.timeline__slider', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.object', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    delay: 1000,
    reset: false,
    interval: 50,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.instagram__header', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.5
  });
  
  sr.reveal('.instagram__content', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 700,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.footer .row > *', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    mobile: false,
    reset: false,
    interval: 100,
    opacity: 0,
    viewFactor: 0.2
  });
})
