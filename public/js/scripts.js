
$(window).on("load", function () {
  
  $('body').css('opacity', 1);

  window.sr = ScrollReveal();

  sr.reveal('.header', { 
    origin: 'left',
    duration: 700,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.front', { 
    origin: 'left',
    duration: 1500,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.front__item-wrapper', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    delay: 700,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.about__item', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.cards-list__header', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.card', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  

  sr.reveal('.concept__header', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.concept__section-header', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.discipline', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__additional-dscr', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__timeline-header > *', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  
  
  sr.reveal('.concept__timeline-line', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__timeline-item', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });
  
  sr.reveal('.concept__date', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });

  sr.reveal('.news-list', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.news', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    delay: 500,
    mobile: false,
    reset: false,
    opacity: 0,
    interval: 50,
    viewFactor: 0.2
  });

  sr.reveal('.timeline__header', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.timeline__subheader', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.timeline__slider', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.object', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100%',
    scale: 1,
    mobile: false,
    delay: 1000,
    reset: false,
    interval: 50,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.instagram__header', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.5
  });
  
  sr.reveal('.instagram__content', { 
    origin: 'left',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '200px',
    scale: 1,
    delay: 700,
    mobile: false,
    reset: false,
    opacity: 0,
    viewFactor: 0.2
  });

  sr.reveal('.footer .row > *', { 
    origin: 'bottom',
    duration: 1000,
    easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
    distance: '100px',
    scale: 1,
    mobile: false,
    reset: false,
    interval: 100,
    opacity: 0,
    viewFactor: 0.2
  });
})

$('[data-fancybox]').fancybox({
  beforeClose: function( instance, current ) {
    $('.article').addClass('article--closing');
    console.log('close');
    setTimeout(function() {
      $('.article').removeClass('article--closing');
    }, 500)
  }
})









$(document).on('click', '.js-anchor', function (event) {
  event.preventDefault();

  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
});

$('.front__wrapper').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: false,
  adaptiveHeight: true,
  dots: true,
  arrows: false,
  autoplay: true,
  customPaging: function (slick, index) {
    var targetImage = slick.$slides.eq(index).attr('data-img');
    return '<div style="background-image: url(' + targetImage + ' ");></div>';
  },
  responsive: [
    {
      breakpoint: 567,
      settings: {
        fade: true
      }
    }
  ]
})


$(window).on('load', function () {
  $('.instagram__content').mCustomScrollbar({
    axis: "x",
    scrollInertia: false,
    scrollInertia: 500,
  });
})


setTimeout(function () {
  
  var number = 6;
  
  if ($(window).width() < 1200) {
    number = 4;
  }
  
  if ($(window).width() < 768) {
    number = 2;
  }
  var itemWidth = $('.instagram__content').outerWidth()/number;
  $('.instagram__item').css('width', itemWidth).css('height', itemWidth);
}, 500)



$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('body').toggleClass('body--fixed');
  $('.menu__content').fadeToggle();
});

$(document).on('click', function (e) {
  var modal = $('.menu, .header');
  if (!modal.is(e.target) &&
    modal.has(e.target).length === 0) {
    $('.menu__btn').removeClass('menu__btn--active');
    $('body').removeClass('body--fixed');
    $('.menu__content').fadeOut();
  }
});









$('.timeline__slider').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  infinite: false,
  responsive: [
    {
      breakpoint: 1499,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 567,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
})

//new fullpage('.wrapper', {
//  autoScrolling: false,
//  navigation: true,
//  navigationPosition: 'right'
//});

$('body').scrollspy({ target: '.aside__nav' })
